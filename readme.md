# Humans, so accidental

Everybody makes mistakes. 
But fear not, because sometimes there are solutions that can help us with those before it's too late.  
Sometimes we use those solutions without even knowing it: ISBN codes, abortion pills (well, not really), credit card numbers.

![We don't really make mistakes](http://i.imgur.com/UqPoBMy.gif)

In case of computers checking stuff, people wrote algorithms.

Today we're going to talk about [check digit algorithms](http://en.wikipedia.org/wiki/Check_digit), which are generally designed to capture human transcription errors.

> "Whatever, what do I have to do this time?" 
> > -Someone, at some point

Nice question. Your job is to figure out whether a credit card number is accurate or just gibberish by using one of these check digit algorithms.

![Some of you, thinking about me](http://i.imgur.com/6frY8Fh.gif)
 
## Aw, damn, how do I do that?
Well, turns out that most of the credit card companies are using an algorithm called [Luhn algorithm](http://en.wikipedia.org/wiki/Luhn_algorithm).
You just have to implement the algorithm, based on its rules. Plus some other simple stuff.

### How do you check if a credit card number is valid?
You have to check its prefix, its length and the check digit criteria.  
Other parameters can be used, such as the CCV, the expire date, etc. These parameters are out of scope. We will just concentrate on the credit card number.

![Boring stuff](http://i.imgur.com/aybLv6O.gif)

#### Cracking the credit card code, a bit of background
A credit card number (sometimes called PAN, Primary Account Number) is usually 16 digits length, sometimes up to 19.  
Its structure is as follows:

- a six-digit Issuer Identification Number (IIN) (previously called the "Bank Identification Number" (BIN)) the first digit of which is the Major Industry Identifier (MII),
- a variable length (up to 12 digits) individual account identifier,
- a single check digit calculated using the Luhn algorithm

The first credit card number is known as the Major Industry Identifier (MII) and it represents the entity which issues the card (e.g. American Express, VISA, MasterCard, etc.).

<!---
![Heavy sheet](http://i.imgur.com/cCp8do2.gif)
-->

## Ok, fine, what do I have to do?
This code challenge will have different levels of difficulty.  
You can implement it as a web service that returns a valid JSON output or as a web application. Or both. It's your choice. 

### Level 1, the beginner: math +5pts
You check if the credit card number is valid, by checking its length and by using the Luhn algorithm.  
This can be checked on form submission either on front-end or back-end.  
Can be as fancy as you want. Like, with input mask and sparkles.

![Your new avatar](http://i.imgur.com/dsME1G7.jpg)

### Level 2: shield +10pts
You check if the entered number (if valid), is part of any of the credit card companies listed below:

- American Express: begins with [ 34 || 37 ]; length 15 digits
- Discover: begins with [ 6011 || 622126...622925 || 644...649 || 65 ]
- Maestro: begins with [ 500000...509999 || 560000...699999 ]; length 12...19 digits
- Master Card: begins with [ 51...55 ]; length 16 digits
- VISA: begins with [ 4 ]; length [ 13 || 16 ] digits

If you implement more than one (i.e. 2 or more) of any of those, you get wisdom +2pts for every extra implementation.  
If you're feeling fancy, you can also find more on the Interwebz.

Hints: regex, GOF's decorator, GOF's chain of command.  
Note: not all the hints might be correct.

![Your new avatar](http://i.imgur.com/1YkdpSn.jpg)
    
### Level 3, the "I can take more than this, Captain!": cracker +20pts
Generate your valid credit card number.  
Ideally, the user needs to be able to choose the credit card issuer from the list above.

![Spooky](http://i.imgur.com/MjeAMig.gif)

If you implement more than one of any of those, you get hacker +2pts for every extra implementation.

![Your new avatar](http://i.imgur.com/2GPF1WD.jpg)

## Five rules to rule them all
- Thou must complete Level 1. The other ones are optional
- Thou shall implement your solution using the language of your choice between Javascript and PHP or both
- Thou canst use any framework thou desire (e.g. jQuery, Laravel, Bootstrap, Yii, etc.), but not external libraries that do the work for thou
- Every achievement is imaginary and not rewarded in any form, which include (but it isn't limited to) real life, virtual, you name it.
- Thou shall submit your solution by the terms of Spyder Trap's Code Challenge
- Each time you complete one level, thou canst use one of the images assigned to it at the end of its description as an avatar wherever you want for as long as you want to. Or just don't.

## Couple notes about code challenge:

1. First, before you do anything else, ensure that you run the following from your
terminal within this repo (or tower, etc...):
    $ git remote set-url --push origin git@spydertrap.git.beanstalkapp.com:/spydertrap/code-challenge-submissions.git
What this will do is to set up a different push destination than pull (so your
code will push to a different repo, which is what we want).
2. Next, see what challenges exist:
    $ git branch -r 
3. Check out the assigned challenge:
    $ git checkout credit_card_verify
4. Branch a personal copy named in the following convention *challenge\_name*\_**yourname**:
    $ git checkout -b credit_card_verify_joe
5. Go to town on your solution, committing to your branch as needed. Don't push
this branch upstream yet!
6. Finally, it's the day for the results to be turned in (or you're taking this
test as a prospective hire and you've completed it, whichever). Push your shiny
new branch up to the repo:
    $ git push -u origin challenge\_name\_yourname
7. Congrats! That's it! You've just submitted your solution!
