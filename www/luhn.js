var counter = 0,
       swingCounter = 0;
   
   
   function getCreditCardType(accountNumber)
   {
   
     //start without knowing the credit card type
     var result = "unknown";
   
     //first check for MasterCard
     if (/^5[1-5]/.test(accountNumber))
     {
       result = "mastercard";
     }
   
     //then check for Visa
     else if (/^4/.test(accountNumber))
     {
       result = "visa";
     }
     
     else if(/^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/.test(accountNumber))
     {
        result = "discover";
     }
   
     //then check for AmEx
     else if (/^3[47]/.test(accountNumber))
     {
       result = "amex";
     }
   
     return result;
   }
   
   function handleEvent(event) {
     var value   = event.target.value,    
         type    = getCreditCardType(value),
         target  = event.target,
         icon = document.getElementById("cc-icon").firstChild;
         
    if(!/^[\d\s]*$/.test(value)){
      if(counter != 1) {
         alert("You've entered a non-numeric character. Please remedy this horendous error immediately.");
         counter = 1;
      }
      else {
         alert("You have failed me for the last time...");
         target.value = "";
         counter = 0;
      }
      return false;
    }
    
   
     switch (type)
     {
       case "mastercard":
         icon.setAttribute("class", "fa fa-4x fa-cc-mastercard");
         break;
   
       case "visa":
         icon.setAttribute("class", "fa fa-4x fa-cc-visa");
         break;
       
       case "discover":
         icon.setAttribute("class", "fa fa-4x fa-cc-discover");
         break;
   
       case "amex":
         icon.setAttribute("class", "fa fa-4x fa-cc-amex");
         break;
   
       default:
         icon.setAttribute("class", "fa fa-4x fa-pied-piper-alt");
     }
     
      function luhnChk(ccNum) {
         ccNum = ccNum.replace(/\s+/g, '');
         var len = ccNum.length,
             mul = 0,
           array = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]],
             sum = 0;
         
         while (len--) {
            sum += array[mul][parseInt(ccNum.charAt(len), 10)];
            mul ^= 1;
         }
         
         return sum % 10 === 0 && sum > 0;
      };
      
      var yoda = document.getElementById('yoda-example');
      var darthVader = document.getElementById('darth-vader-example');
      
      if(value.length == 0){
         resetSabers();
      }
      
      if(value.length > 12){
         console.log(luhnChk(value));
         if(luhnChk(value)){
            yoda.checked = true;
            darthVader.checked = false;
            lightWin();
         }
         else {
            yoda.checked = false;
            darthVader.checked = true;
            darkWin();
         }
      }
      else {
       yoda.checked = true;
       darthVader.checked = true;
       swingCounter++;
       if(((swingCounter) % 4 == 0) || swingCounter == 1){
          var random1 = Math.floor((Math.random() * 45) + 30),
              random2 = -Math.floor((Math.random() * 45) + 30);
          swingSabers(random1, random2);
       }
      }
   }
   
   
   
   document.addEventListener("DOMContentLoaded", function(){
     var textbox = document.getElementById("cc-num");
     textbox.addEventListener("keyup", handleEvent, false);
     textbox.addEventListener("blur", handleEvent, false);
   }, false);
   
   function swingSabers(lightRand, darkRand) {
      var element = $('.example-item.left .lightsaber');
      var yodaPlasma = $('.example-item.left .lightsaber').find('.plasma');
      var elementRight = $('.example-item.right .lightsaber');
      var vaderPlasma = $('.example-item.right .lightsaber').find('.plasma');
      var tween1;
      var tween2;
      var tween0 = new TweenLite(element, 0.3, {transform:'rotateZ(-20deg)', left:0, transformOrigin:"bottom", ease: Sine.easeInOut, delay:0.1});
         tween1 = new TweenLite(element, 0.5, {transform:'rotateZ('+lightRand+'deg)', transformOrigin:"bottom", ease: Power1.easeIn, delay:0.4});
         tween3 = new TweenLite(elementRight, 0.3, {rotationZ:"20deg", right: 0, transformOrigin:"bottom", delay:0.1, ease: Sine.easeInOut});
         tween2 = new TweenLite(elementRight, 0.5, {rotationZ: darkRand+"deg", transformOrigin:"bottom", delay:0.4, ease: Power1.easeIn});
   }
   function resetSabers(){
      var element = $('.example-item.left .lightsaber');
      var elementRight = $('.example-item.right .lightsaber');
      var tween0 = new TweenLite(element, 1, {transform:'rotateZ(0deg)', transformOrigin:"bottom", ease: Power1.easeInOut, delay:0.1});
      var tween1 = new TweenLite(elementRight, 1, {transform:'rotateZ(0deg)', transformOrigin:"bottom", ease: Power1.easeIn, delay:0.1});
   }
   function lightWin(){
      var element = $('.example-item.left .lightsaber');
      var elementRight = $('.example-item.right .lightsaber');
      var tween0 = new TweenLite(element, 0.3, {rotationZ:"-30deg", transformOrigin:"bottom", ease: Power1.easeOut});
      var tween3 = new TweenLite(element, 0.6, {rotationZ:"50deg", left:100, transformOrigin:"bottom", ease: Power1.easeInOut, delay:0.4});
      var tween4 = new TweenLite(element, 1, {transform:'rotateZ(0deg)', left: 0, transformOrigin:"bottom", ease: Power1.easeInOut, delay:2.5});
      var tween1 = new TweenLite(elementRight, 0.7, {transform:'rotateZ(0deg)', right: 0, transformOrigin:"bottom", ease: Power1.easeIn, delay:0.1});
   }
   function darkWin(){
      var element = $('.example-item.left .lightsaber');
      var elementRight = $('.example-item.right .lightsaber');
      var tween0 = new TweenLite(elementRight, 0.5, {rotationZ:"30deg", transformOrigin:"bottom", ease: Power1.easeOut});
      var tween3 = new TweenLite(elementRight, 0.6, {rotationZ:"-50deg", right: 100, transformOrigin:"bottom", ease: Power1.easeInOut, delay:0.6});
      var tween4 = new TweenLite(elementRight, 1, {transform:'rotateZ(0deg)', right: 0, transformOrigin:"bottom", ease: Power1.easeInOut, delay:2.5});
      var tween1 = new TweenLite(element, 0.7, {transform:'rotateZ(0deg)', left: 0, transformOrigin:"bottom", ease: Power1.easeIn, delay:0.1});
   }